'use strict';
//DOCUMENTATION
//https://openweathermap.org/api

document.addEventListener('DOMContentLoaded', ()=>{
    const weatherObj = {
        APIKey : '9fc7c03598b17901376decfd4745f46b',
        coordsEndpoint : `http://api.openweathermap.org/geo/1.0/direct`,
        cityInput : document.querySelector('#city input'),
        countrySelect : document.querySelector('select'),
        weatherEndpoint : `http://api.openweathermap.org/data/2.5/forecast`,
        weatherData : null,
        weatherButton : document.querySelector('button'),
        output : {
            lat : document.querySelector('#lat input'),
            long : document.querySelector('#long input'),
            population : document.getElementById('pop'),
            sunrise : document.getElementById('rise'),
            sunset : document.getElementById('set'),
            cards : {
                time : document.querySelectorAll('.time'),
                icon : document.querySelectorAll('.icon'),
                desc : document.querySelectorAll('.desc'),
                temp : document.querySelectorAll('.temp'),
                feelsLike : document.querySelectorAll('.feels-like'),
                humidity : document.querySelectorAll('.humidity'),
                wind : document.querySelectorAll('.wind'),
            },
        },
        getCoords(){
            const city = this.cityInput.value;
            const url = this.coordsEndpoint + `?q=${city},,&limit=5&appid=${this.APIKey}&units=metric`
            fetch(url)
                .then(resp => {
                    if(resp.status == 200)
                        return resp.json();
                    else
                        console.log("request failed")
                })
                .then(data => this.updateCountries(data))
                .catch(err => console.log(err.message))
        },
        updateCountries(data){
            data.forEach(obj => createElem('option', {textContent : obj.country, value : [obj.lat, obj.lon]}, this.countrySelect))
            this.weatherButton.disabled = false;
        },
        getWeatherData(){
            const coords = this.countrySelect.value.split(',');
            this.output.lat.value = coords[0];
            this.output.long.value = coords[1];

            const url = this.weatherEndpoint + `?lat=${coords[0]}&lon=${coords[1]}&appid=${this.APIKey}`

            fetch(url)
                .then(resp => {
                    if(resp.status === 200)
                        return resp.json();
                    else
                        console.log("request failed", resp.status);
                })
                .then(data => this.updateWeatherData(data))
                .catch(err => console.log(err.message))
        },
        updateWeatherData(data){
            this.output.population.value = data.city.population;
            this.output.sunrise.value = dateInSecondsToString(data.city.sunrise).split(' ')[1];
            this.output.sunset.value = dateInSecondsToString(data.city.sunset).split(' ')[1];
            for(let i = 0; i < this.output.cards.time.length; i++){
                this.populateCard(data, i)
            }
        },
        populateCard(data, i){
            this.output.cards.time[i].textContent = dateInSecondsToString(data.list[i * 8].dt);
            this.output.cards.icon[i].src = `https://openweathermap.org/img/wn/${data.list[i * 8].weather[0].icon}@2x.png`;
            this.output.cards.desc[i].textContent = data.list[i * 8].weather[0].description;
            this.output.cards.temp[i].textContent = data.list[i * 8].main.temp;
            this.output.cards.feelsLike[i].textContent = data.list[i * 8].main.feels_like;
            this.output.cards.humidity[i].textContent = data.list[i * 8].main.humidity
            this.output.cards.wind[i].textContent = data.list[i * 8].wind.speed;
        }
    }

    weatherObj.cityInput.addEventListener('blur', () => weatherObj.getCoords());

    weatherObj.weatherButton.addEventListener('click', (e) => {e.preventDefault(); weatherObj.getWeatherData()});
    weatherObj.weatherButton.disabled = true;
});

function createElem(tag, properties, parent){
    const elem = document.createElement(tag);
    Object.keys(properties).forEach(prop => elem[prop] = properties[prop])
    parent.appendChild(elem);
    return elem;
}

function dateInSecondsToString(s){
    const date = new Date(s * 1000);
    return `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(2, '0')}-${String(date.getDate()).padStart(2, '0')} ${String(date.getHours()).padStart(2, '0')}:${String(date.getMinutes()).padStart(2, '0')}`;
}